const assert = require('assert');
let retailLogin = 'd.mazurenko@gepur.org';
let retailPassword = 'pKTDMGAXbqjT';
let wholesaleLogin = 'gepur-test-opt@ukr.net';
let wholesalePassword = 'pKTDMGAXbqjT';
let linkToFollow = [
  'https://gepur.com/catalog/aksessuary',
  'https://en.gepur.com/catalog/aksessuary',
  'https://ua.gepur.com/catalog/aksessuary'
];
let commentText = '!!!TEST!!!';
let resultUrl = [
  'https://gepur.com/create-order/create-order',
  'https://en.gepur.com/create-order/create-order',
  'https://ua.gepur.com/create-order/create-order'
];
let pageUrl;

async function clickingQuantityButton(quantity){
  for (var i = 1; i <= quantity; ++i){
    let quantityPlusBtn = await $('//*[@id="app"]/div/div[2]/div/div/div[2]/div/div/div[2]/div[4]/div/button[2]/img');
    quantityPlusBtn.click();
  }
};
async function authentificateUser(login, password){
  let loginField = await $('#loginform-email');
  await loginField.setValue(login);
  let passwordField = await $('#loginform-password');
  await passwordField.setValue(password);
  let LoginBtn = await $('body > main > section > div > div.checkout__right--form > div.tabs-container.cart-tab-container > div.tab.tab-cart.animated.slideInUp.tab-item0 > div.checkout-itog > div.bb__g > button');
  await LoginBtn.scrollIntoView();
  await LoginBtn.click();
}
async function chooseProductRetail(link){
  await browser.url(link);
  let slider = await $('//*[@id="app"]/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a/img');
  await slider.click();
  await browser.pause(300);
  await clickingQuantityButton(1);
  let addToCartBtn = await $('//*[@id="app"]/div/div[2]/div/div/div[2]/div/div/div[2]/div[6]/div/button/span');
  await addToCartBtn.scrollIntoView();
  await addToCartBtn.click();
  let createOrderBtn = await $('//*[@id="bucket--scroll-part"]/div[2]/div[2]/a');
  await createOrderBtn.click();
}
async function chooseProductWholesale(link){
  await browser.url(link);
  let slider = await $('//*[@id="app"]/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a/img');
  await slider.click();
  await browser.pause(300);
  await clickingQuantityButton(4);
  let addToCartBtn = await $('//*[@id="app"]/div/div[2]/div/div/div[2]/div/div/div[2]/div[6]/div[1]/button/span');
  await addToCartBtn.scrollIntoView();
  await addToCartBtn.click();
  let createOrderBtn = await $('//*[@id="bucket--scroll-part"]/div[2]/div[2]/a');
  await createOrderBtn.click();
}
async function proceedCheckoutStep1(){
  let userPhone = await $('//*[@id="customerform-phone"]');
  await userPhone.scrollIntoView();
  let createOrderBtn2 = await $('/html/body/main/section/div/div[3]/div[2]/div[4]/div[2]/button');
  await browser.pause(100);
  await createOrderBtn2.scrollIntoView();
  await createOrderBtn2.click();
}
async function proceedCheckoutStep2(){
  await browser.pause(1000);
  let NovaPochtaCheck = await $('/html/body/main/div[1]/div[2]/div[3]/div[3]/div[2]/div[1]/div/div[2]/div[1]/div[1]/input');
  await NovaPochtaCheck.scrollIntoView();
  await NovaPochtaCheck.click();
  await browser.pause(500);
  let NovaPochtaOffice = await $('//*[@id="deliveryform-address"]');
  await browser.pause(500);
  await NovaPochtaOffice.scrollIntoView();
  await NovaPochtaOffice.setValue('1');
  let createOrderBtn3 = await $('/html/body/main/div[1]/div[2]/div[3]/div[3]/div[2]/div[3]/div/button');
  await createOrderBtn3.scrollIntoView();
  await createOrderBtn3.click();
}
async function proceedCheckoutStep3(){
  await browser.pause(1000);
  let pBCheck = await $('//html/body/main/div/div[2]/div[3]/div[4]/div[2]/div[1]/div/div[2]/div[2]/div[1]/input');
  await browser.pause(500);
  await pBCheck.scrollIntoView();
  await pBCheck.click();
  let addCommentTextBttn = await $('/html/body/main/div/div[2]/div[3]/div[4]/div[3]/div[3]/div/span');
  await addCommentTextBttn.scrollIntoView();
  await addCommentTextBttn.click();
  let addCommentTextTextField = await $('/html/body/main/div/div[2]/div[3]/div[4]/div[3]/div[5]/textarea');
  await addCommentTextTextField.scrollIntoView();
  await addCommentTextTextField.setValue(commentText);
  await browser.pause(300);
  let finalSubmitBtn = await $('/html/body/main/div/div[2]/div[3]/div[4]/div[3]/div[3]/input');
  await browser.pause(300);
  await finalSubmitBtn.scrollIntoView();
  await finalSubmitBtn.click();
}
async function getResultUrl(){
  await browser.pause(500);
  pageUrl = await browser.getUrl();
}

describe('Десктоп. Чекаут из карточки товара. Новая Почта. Приват Банк', () => {

  beforeEach(async ()=> {
    await browser.reloadSession();
    await browser.setWindowSize(1024,768);
  });

  it('RU. Розница', async() => {
    await chooseProductRetail(linkToFollow[0]);
    await authentificateUser(retailLogin, retailPassword);
    await proceedCheckoutStep1();
    await proceedCheckoutStep2();
    await proceedCheckoutStep3();
    await getResultUrl();
    assert.equal(pageUrl, resultUrl[0]);
  });
    
  it('RU. Опт', async() => {
    await chooseProductWholesale(linkToFollow[0]);
    await authentificateUser(wholesaleLogin, wholesalePassword);
    await proceedCheckoutStep1();
    await proceedCheckoutStep2();
    await proceedCheckoutStep3();
    await getResultUrl();
    assert.equal(pageUrl, resultUrl[0]);
  });

  it('EN. Розница', async() => {
    await chooseProductRetail(linkToFollow[1]);
    await authentificateUser(retailLogin, retailPassword);
    await proceedCheckoutStep1();
    await proceedCheckoutStep2();
    await proceedCheckoutStep3();
    await getResultUrl();
    assert.equal(pageUrl, resultUrl[1]);
  });

  it('EN. Опт', async() => {
    await chooseProductWholesale(linkToFollow[1]);
    await authentificateUser(wholesaleLogin, wholesalePassword);
    await proceedCheckoutStep1();
    await proceedCheckoutStep2();
    await proceedCheckoutStep3();
    await getResultUrl();
    assert.equal(pageUrl, resultUrl[1]);
  });

  it('UA. Розница', async() => {
    await chooseProductRetail(linkToFollow[2]);
    await authentificateUser(retailLogin, retailPassword);
    await proceedCheckoutStep1();
    await proceedCheckoutStep2();
    await proceedCheckoutStep3();
    await getResultUrl();
    assert.equal(pageUrl, resultUrl[2]);
  });
    
  it('UA. Опт', async() => {
    await chooseProductWholesale(linkToFollow[2]);
    await authentificateUser(wholesaleLogin, wholesalePassword);
    await proceedCheckoutStep1();
    await proceedCheckoutStep2();
    await proceedCheckoutStep3();
    await getResultUrl();
    assert.equal(pageUrl, resultUrl[2]);
  });
});