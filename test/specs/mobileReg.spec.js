const assert = require('assert');

const startUrl = [
  'https://m.gepur.com',
  'https://m.gepur.com'
];
const finalUrl = [
  'https://m.gepur.com',
  'https://m.gepur.com'
];
const referenceUrl = [
  'https://m.gepur.com',
  'https://m.gepur.com'
];
const fioDef = [
  'ROZNITSA TEST TEST',
  'OPT TEST TEST'
];

const emailDef = [
  'm_' + 'd.mazurenko' + Math.floor((Math.random() * 99999999)) + '@gepur.org',
  'm_' + 'gepur-test-opt' + Math.floor((Math.random() * 99999999)) + '@ukr.net'
];

const passwordDef = [
  'Fklg64sd',
  'Fklg64sd'
];

let element;
const mainMenuBtn = '//*[@id="root"]/div/header/div[1]';
const mainMenuCloseBtn = '#root > div > header > div:nth-child(4) > aside > div.g-menu__navbar > div > button.btn__h.btn__h--active';
const userAccauntBtn = '=Личный кабинет';
const regTabBtn = '#root > div > div.responsive-content__cmt > div > div > div.a-cnt__tabs.clearfix > ul > li:nth-child(2) > span';
const fioInput = '#user-fio';
const emailInput = '#user-sign-up-email';
const passwordInput = 'html body div.page-wrapper div#root.content div div.responsive-content__cmt div.a-cmt div.a-cnt div.a-cnt__tabs.clearfix div.a-cnt__tabs-content.clearfix div.a-tabs__item.a-tabs__list--active div.form-cnt form div.f-g input#user-password.g-i.i-s';
const userTypeRadio = [
  '#root > div > div.responsive-content__cmt > div > div > div.a-cnt__tabs.clearfix > div > div.a-tabs__item.a-tabs__list--active > div > form > div:nth-child(5) > label',
  '#root > div > div.responsive-content__cmt > div > div > div.a-cnt__tabs.clearfix > div > div.a-tabs__item.a-tabs__list--active > div > form > div:nth-child(4) > label'
];
const regBtn = '//*[@id="root"]/div/div[4]/div/div/div[2]/div/div[2]/div/form/div[6]/div/button';


async function performReg(url, fio, email, password, radio, refUrl) {
  try {

    await browser.url(url);
    const headerBtnInner = await $(mainMenuBtn);
    await headerBtnInner.click();
    const userAccBtnInner = await $(userAccauntBtn);
    await userAccBtnInner.click();
    const heaserCloseBtnInner = await $(mainMenuCloseBtn);
    const regTabBtnInner = await $(regTabBtn);
    await regTabBtnInner.click();
    await heaserCloseBtnInner.click();
    await browser.pause(1000);
    const fioInputInner = await $(fioInput);
    await fioInputInner.setValue(fio);
    const emailInputInner = await $(emailInput);
    await emailInputInner.setValue(email);
    const passwordInputInner = await $(passwordInput);
    await passwordInputInner.setValue(password);
    const radioInner = await $(radio);
    await radioInner.click();
    const regBtnInner = await $(regBtn);
    await regBtnInner.click();

  } catch (error) {
      throw(error);
  }
}

describe('Мобильный. Регистрация.', () => {
  beforeEach(async ()=> {
    await browser.reloadSession();
    //await browser.setWindowSize(414 ,1007);
    await browser.setWindowSize(776, 1007);
  });

  it('Розница.', async () => {
    await performReg(startUrl[0], fioDef[0], emailDef[0], passwordDef[0], userTypeRadio[0], referenceUrl[0]);
  });

  it('Опт.', async () => {
    await performReg(startUrl[1], fioDef[1], emailDef[1], passwordDef[1], userTypeRadio[1], referenceUrl[1]);
  });
});