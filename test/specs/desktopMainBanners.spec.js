const assert = require('assert');
let desktopPage = [
  'https://gepur.com/',
  'https://en.gepur.com/',
  'https://ua.gepur.com'
];
let title, banner, link; 
let bannerId = [
  '//*[@id="app"]/div/div[2]/div[1]/div[1]/a/img',
  '//*[@id="app"]/div/div[2]/div[1]/div[2]/a/img',
  '//*[@id="app"]/div/div[2]/div[1]/div[3]/a/img',
  '//*[@id="app"]/div/div[2]/div[3]/div[1]/a/img',
  '//*[@id="app"]/div/div[2]/div[3]/div[2]/a/img',
  '//*[@id="app"]/div/div[2]/div[3]/div[3]/a/img',
  '//*[@id="app"]/div/div[2]/div[3]/div[4]/a/img',
  '//*[@id="app"]/div/div[2]/div[3]/div[5]/a/img',
  '//*[@id="app"]/div/div[2]/div[3]/div[6]/a/img',
  '//*[@id="app"]/div/div[2]/div[4]/div[1]/a/img',
  '//*[@id="app"]/div/div[2]/div[4]/div[2]/a/img',
  '//*[@id="app"]/div/div[2]/div[5]/div[1]/a/img',
  '//*[@id="app"]/div/div[2]/div[5]/div[2]/a/img'
];
let ruResultPage = [
  'https://gepur.com/catalog/novinki?main_banner_1_041019',
  'https://gepur.com/catalog/platya?main_banner_2_041019',
  'https://gepur.com/bestidea/sedokova2019?main_banner_3_041019',
  'https://gepur.com/catalog/bluzy?main_banner_4_041019',
  'https://gepur.com/catalog/bryuki-leggensy-shorty?main_banner_5_041019',
  'https://gepur.com/catalog/kostyumy-i-komplekty?main_banner_6_041019',
  'https://gepur.com/catalog/yubki?main_banner_7_041019',
  'https://gepur.com/catalog/obuv?main_banner_8_041019',
  'https://gepur.com/catalog/sumki-klatchi-koshelki?main_banner_9_041019',
  'https://gepur.com/catalog/verhnyaya-odezhda?main_banner_10_041019',
  'https://gepur.com/bestidea/gepur-design?main_banner_11_041019',
  'https://gepur.com/catalog/futbolki-bluzy-svitera?main_banner_12_041019',
  'https://gepur.com/bestidea/clothes-for-every-day-2018?main_banner_13_041019'
];
let enResultPage = [
  'https://en.gepur.com/catalog/novinki?main_banner_1_041019',
  'https://en.gepur.com/catalog/platya?main_banner_2_041019',
  'https://en.gepur.com/bestidea/sedokova2019?main_banner_3_041019',
  'https://en.gepur.com/catalog/bluzy?main_banner_4_041019',
  'https://en.gepur.com/catalog/bryuki-leggensy-shorty?main_banner_5_041019',
  'https://en.gepur.com/catalog/kostyumy-i-komplekty?main_banner_6_041019',
  'https://en.gepur.com/catalog/yubki?main_banner_7_041019',
  'https://en.gepur.com/catalog/obuv?main_banner_8_041019',
  'https://en.gepur.com/catalog/sumki-klatchi-koshelki?main_banner_9_041019',
  'https://en.gepur.com/catalog/verhnyaya-odezhda?main_banner_10_041019',
  'https://en.gepur.com/bestidea/gepur-design?main_banner_11_041019',
  'https://en.gepur.com/catalog/futbolki-bluzy-svitera?main_banner_12_041019',
  'https://en.gepur.com/bestidea/clothes-for-every-day-2018?main_banner_13_041019'
];
let uaResultPage = [
  'https://ua.gepur.com/catalog/novinki?main_banner_1_041019',
  'https://ua.gepur.com/catalog/platya?main_banner_2_041019',
  'https://ua.gepur.com/bestidea/sedokova2019?main_banner_3_041019',
  'https://ua.gepur.com/catalog/bluzy?main_banner_4_041019',
  'https://ua.gepur.com/catalog/bryuki-leggensy-shorty?main_banner_5_041019',
  'https://ua.gepur.com/catalog/kostyumy-i-komplekty?main_banner_6_041019',
  'https://ua.gepur.com/catalog/yubki?main_banner_7_041019',
  'https://ua.gepur.com/catalog/obuv?main_banner_8_041019',
  'https://ua.gepur.com/catalog/sumki-klatchi-koshelki?main_banner_9_041019',
  'https://ua.gepur.com/catalog/verhnyaya-odezhda?main_banner_10_041019',
  'https://ua.gepur.com/bestidea/gepur-design?main_banner_11_041019',
  'https://ua.gepur.com/catalog/futbolki-bluzy-svitera?main_banner_12_041019',
  'https://ua.gepur.com/bestidea/clothes-for-every-day-2018?main_banner_13_041019'
];
let pageTitle = [
  "Интернет магазин женской одежды от производителя | Gepur",
  "Online store of women's clothing from the manufacturer | GEPUR",
  "Інтернет магазин жіночого одягу від виробника в Україні | Gepur"
];
let linkToFollow = [
  'https://gepur.com',
  'https://en.gepur.com',
  'https://ua.gepur.com'
];

async function getPageTitle(url){
  await browser.url(url);
  title = await browser.getTitle();
};

async function pressPageDown(num){
  for(let i = 0; i <= num; ++i){
    await browser.keys('PageDown');
  }
}

try{
  describe('Десктоп', () => {
    describe('Главная страница', () => {
      describe('Страницы имеют правильный title', async () => {
  
        beforeEach(async () => {
            await browser.reloadSession();
            await browser.setWindowSize(1024, 768);
            await browser.pause(300);
        });
  
        it('gepur.com имеет правильный title', async () => {
          await getPageTitle(desktopPage[0]);
          assert.equal(title, pageTitle[0]);
        });
  
        it('EN.gepur.com имеет правильный title', async () => {
          await getPageTitle(desktopPage[1]);
          assert.equal(title, pageTitle[1]);
        });
  
        it('UA.gepur.com имеет правильный title', async () => {
          await getPageTitle(desktopPage[2]);
          assert.equal(title, pageTitle[2]);
        });
      });
  
      describe('Баннеры. RU', async () => {
  
        beforeEach(async () => {
          await browser.reloadSession();
          await browser.setWindowSize(1024, 768);
          await browser.url(linkToFollow[0]);
          await browser.pause(300);
        });
  
        it('"Новинки" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[0]);
          await pressPageDown(2);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, ruResultPage[0]);
        });
  
        it('"Платья" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[1]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, ruResultPage[1]);
        });
  
        it('"Анна Седокова" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[2]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, ruResultPage[2]);
        });
  
        it('"Блузы" ведет по правильному адресу', async () => {
          let bannerTwo= await $(bannerId[3]);
          await pressPageDown(1);
          await bannerTwo.scrollIntoView();
          await bannerTwo.click();
          await browser.pause(500);
          let link = await bannerTwo.getUrl();
          await assert.equal(link, ruResultPage[3]);
        });
  
        it('"Брюки" ведет по правильному адресу', async () => {
          let bannerThree= await $(bannerId[4]);
          await pressPageDown(1);
          await bannerThree.scrollIntoView();
          await bannerThree.click();
          await browser.pause(500);
          let link = await bannerThree.getUrl();
          await assert.equal(link, ruResultPage[4]);
        });
  
        it('"Костюмы, Комплекты" ведет по правильному адресу', async () => {
          let bannerFour= await $(bannerId[5]);
          await pressPageDown(1);
          await bannerFour.scrollIntoView();
          await bannerFour.click();
          await browser.pause(500);
          let link = await bannerFour.getUrl();
          await assert.equal(link, ruResultPage[5]);
        });
  
        it('"Юбки" ведет по правильному адресу', async () => {
          let bannerFive= await $(bannerId[6]);
          await pressPageDown(1);
          await bannerFive.scrollIntoView();
          await bannerFive.click();
          await browser.pause(500);
          let link = await bannerFive.getUrl();
          await assert.equal(link, ruResultPage[6]);
        });
  
        it('"Обувь" ведет по правильному адресу', async () => {
          let bannerSix= await $(bannerId[7]);
          await pressPageDown(1);
          await bannerSix.scrollIntoView();
          await bannerSix.click();
          await browser.pause(500);
          let link = await bannerSix.getUrl();
          await assert.equal(link, ruResultPage[7]);
        });
  
        it('"Сумки, клатчи" ведет по правильному адресу', async () => {
          let bannerSeven= await $(bannerId[8]);
          await pressPageDown(1);
          await bannerSeven.scrollIntoView();
          await bannerSeven.click();
          await browser.pause(500);
          let link = await bannerSeven.getUrl();
          await assert.equal(link, ruResultPage[8]);
        });
  
        it('"Верхняя одежда" ведет по правильному адресу', async () => {
          let bannerEight= await $(bannerId[9]);
          await pressPageDown(4);
          await bannerEight.scrollIntoView();
          await bannerEight.click();
          await browser.pause(500);
          let link = await bannerEight.getUrl();
          await assert.equal(link, ruResultPage[9]);
        });
  
        it('"Gepur Design" ведет по правильному адресу', async () => {
          let bannerNine= await $(bannerId[10]);
          await pressPageDown(4);
          await bannerNine.scrollIntoView();
          await bannerNine.click();
          await browser.pause(500);
          let link = await bannerNine.getUrl();
          await assert.equal(link, ruResultPage[10]);
        });
  
        it('"Knitwear" ведет по правильному адресу', async () => {
          let bannerTen= await $(bannerId[11]);
          await pressPageDown(4);
          await bannerTen.scrollIntoView();
          await bannerTen.click();
          await browser.pause(500);
          let link = await bannerTen.getUrl();
          await assert.equal(link, ruResultPage[11]);
        });
  
        it('"На каждый день" ведет по правильному адресу', async () => {
          let bannerEleven= await $(bannerId[12]);
          await pressPageDown(4);
          await bannerEleven.scrollIntoView();
          await bannerEleven.click();
          await browser.pause(500);
          let link = await bannerEleven.getUrl();
          await assert.equal(link, ruResultPage[12]);
        });
      });
  
      describe.skip('Баннеры. EN', async () => {
  
        beforeEach(async () => {
          await browser.reloadSession();
          await browser.setWindowSize(1024, 768);
          await browser.url(linkToFollow[1]);
          await browser.pause(300);
        });
  
        it('"Новинки" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[0]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, enResultPage[0]);
        });
  
        it('"Платья" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[1]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, enResultPage[1]);
        });
  
        it('"Анна Седокова" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[2]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, enResultPage[2]);
        });
  
        it('"Блузы" ведет по правильному адресу', async () => {
          let bannerTwo= await $(bannerId[3]);
          await pressPageDown(1);
          await bannerTwo.scrollIntoView();
          await bannerTwo.click();
          await browser.pause(500);
          let link = await bannerTwo.getUrl();
          await assert.equal(link, enResultPage[3]);
        });
  
        it('"Брюки" ведет по правильному адресу', async () => {
          let bannerThree= await $(bannerId[4]);
          await pressPageDown(1);
          await bannerThree.scrollIntoView();
          await bannerThree.click();
          await browser.pause(500);
          let link = await bannerThree.getUrl();
          await assert.equal(link, enResultPage[4]);
        });
  
        it('"Костюмы, Комплекты" ведет по правильному адресу', async () => {
          let bannerFour= await $(bannerId[5]);
          await pressPageDown(1);
          await bannerFour.scrollIntoView();
          await bannerFour.click();
          await browser.pause(500);
          let link = await bannerFour.getUrl();
          await assert.equal(link, enResultPage[5]);
        });
  
        it('"Юбки" ведет по правильному адресу', async () => {
          let bannerFive= await $(bannerId[6]);
          await pressPageDown(1);
          await bannerFive.scrollIntoView();
          await bannerFive.click();
          await browser.pause(500);
          let link = await bannerFive.getUrl();
          await assert.equal(link, enResultPage[6]);
        });
  
        it('"Обувь" ведет по правильному адресу', async () => {
          let bannerSix= await $(bannerId[7]);
          await pressPageDown(1);
          await bannerSix.scrollIntoView();
          await bannerSix.click();
          await browser.pause(500);
          let link = await bannerSix.getUrl();
          await assert.equal(link, enResultPage[7]);
        });
  
        it('"Сумки, клатчи" ведет по правильному адресу', async () => {
          let bannerSeven= await $(bannerId[8]);
          await pressPageDown(1);
          await bannerSeven.scrollIntoView();
          await bannerSeven.click();
          await browser.pause(500);
          let link = await bannerSeven.getUrl();
          await assert.equal(link, enResultPage[8]);
        });
  
        it('"Верхняя одежда" ведет по правильному адресу', async () => {
          let bannerEight= await $(bannerId[9]);
          await pressPageDown(3);
          await bannerEight.scrollIntoView();
          await bannerEight.click();
          await browser.pause(500);
          let link = await bannerEight.getUrl();
          await assert.equal(link, enResultPage[9]);
        });
  
        it('"Gepur Design" ведет по правильному адресу', async () => {
          let bannerNine= await $(bannerId[10]);
          await pressPageDown(3);
          await bannerNine.scrollIntoView();
          await bannerNine.click();
          await browser.pause(500);
          let link = await bannerNine.getUrl();
          await assert.equal(link, enResultPage[10]);
        });
  
        it('"Knitwear" ведет по правильному адресу', async () => {
          let bannerTen= await $(bannerId[11]);
          await pressPageDown(3);
          await bannerTen.scrollIntoView();
          await bannerTen.click();
          await browser.pause(500);
          let link = await bannerTen.getUrl();
          await assert.equal(link, enResultPage[11]);
        });
  
        it('"На каждый день" ведет по правильному адресу', async () => {
          let bannerEleven= await $(bannerId[12]);
          await pressPageDown(3);
          await bannerEleven.scrollIntoView();
          await bannerEleven.click();
          await browser.pause(500);
          let link = await bannerEleven.getUrl();
          await assert.equal(link, enResultPage[12]);
        });
      });
  
      describe.skip('Баннеры. UA', async () => {
  
        beforeEach(async () => {
          await browser.reloadSession();
          await browser.setWindowSize(1024, 768);
          await browser.url(linkToFollow[2]);
          await browser.pause(300);
        });
        it('"Новинки" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[0]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, uaResultPage[0]);
        });
  
        it('"Платья" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[1]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, uaResultPage[1]);
        });
  
        it('"Анна Седокова" ведет по правильному адресу', async () => {
          let bannerOne= await $(bannerId[2]);
          await pressPageDown(1);
          await bannerOne.scrollIntoView();
          await bannerOne.click();
          await browser.pause(500);
          let link = await bannerOne.getUrl();
          await assert.equal(link, uaResultPage[2]);
        });
  
        it('"Блузы" ведет по правильному адресу', async () => {
          let bannerTwo= await $(bannerId[3]);
          await pressPageDown(1);
          await bannerTwo.scrollIntoView();
          await bannerTwo.click();
          await browser.pause(500);
          let link = await bannerTwo.getUrl();
          await assert.equal(link, uaResultPage[3]);
        });
  
        it('"Брюки" ведет по правильному адресу', async () => {
          let bannerThree= await $(bannerId[4]);
          await pressPageDown(1);
          await bannerThree.scrollIntoView();
          await bannerThree.click();
          await browser.pause(500);
          let link = await bannerThree.getUrl();
          await assert.equal(link, uaResultPage[4]);
        });
  
        it('"Костюмы, Комплекты" ведет по правильному адресу', async () => {
          let bannerFour= await $(bannerId[5]);
          await pressPageDown(1);
          await bannerFour.scrollIntoView();
          await bannerFour.click();
          await browser.pause(500);
          let link = await bannerFour.getUrl();
          await assert.equal(link, uaResultPage[5]);
        });
  
        it('"Юбки" ведет по правильному адресу', async () => {
          let bannerFive= await $(bannerId[6]);
          await pressPageDown(1);
          await bannerFive.scrollIntoView();
          await bannerFive.click();
          await browser.pause(500);
          let link = await bannerFive.getUrl();
          await assert.equal(link, uaResultPage[6]);
        });
  
        it('"Обувь" ведет по правильному адресу', async () => {
          let bannerSix= await $(bannerId[7]);
          await pressPageDown(1);
          await bannerSix.scrollIntoView();
          await bannerSix.click();
          await browser.pause(500);
          let link = await bannerSix.getUrl();
          await assert.equal(link, uaResultPage[7]);
        });
  
        it('"Сумки, клатчи" ведет по правильному адресу', async () => {
          let bannerSeven= await $(bannerId[8]);
          await pressPageDown(1);
          await bannerSeven.scrollIntoView();
          await bannerSeven.click();
          await browser.pause(500);
          let link = await bannerSeven.getUrl();
          await assert.equal(link, uaResultPage[8]);
        });
  
        it('"Верхняя одежда" ведет по правильному адресу', async () => {
          let bannerEight= await $(bannerId[9]);
          await pressPageDown(3);
          await bannerEight.scrollIntoView();
          await bannerEight.click();
          await browser.pause(500);
          let link = await bannerEight.getUrl();
          await assert.equal(link, uaResultPage[9]);
        });
  
        it('"Gepur Design" ведет по правильному адресу', async () => {
          let bannerNine= await $(bannerId[10]);
          await pressPageDown(3);
          await bannerNine.scrollIntoView();
          await bannerNine.click();
          await browser.pause(500);
          let link = await bannerNine.getUrl();
          await assert.equal(link, uaResultPage[10]);
        });
  
        it('"Knitwear" ведет по правильному адресу', async () => {
          let bannerTen= await $(bannerId[11]);
          await pressPageDown(3);
          await bannerTen.scrollIntoView();
          await bannerTen.click();
          await browser.pause(500);
          let link = await bannerTen.getUrl();
          await assert.equal(link, uaResultPage[11]);
        });
  
        it('"На каждый день" ведет по правильному адресу', async () => {
          let bannerEleven= await $(bannerId[12]);
          await pressPageDown(3);
          await bannerEleven.scrollIntoView();
          await bannerEleven.click();
          await browser.pause(500);
          let link = await bannerEleven.getUrl();
          await assert.equal(link, uaResultPage[12]);
        });
      });
    }); 
  });
}catch(error){
  throw(error);
}
