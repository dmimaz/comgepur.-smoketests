const assert = require('assert');
const fetch = require("node-fetch");
const request = require('request');

const pageToFollow = [
    'https://gepur.com',
    'https://en.gepur.com',
    'https://ua.gepur.com',
    'https://m.gepur.com'
];

const mainBanner = [
    '//*[@id="r_header"]/header/span/a/div',
    //'//*[@id="root"]/div/header/div[2]/a' --- ПОСЛЕ НГ закомментить след. строку и раскомментить эту
    '//*[@id="root"]/div/header/div[3]'
];

let response, result;

const statusCodeOk = [
    '200',
]

async function clickingMainBanner(page, banner){
    await browser.url(page);
    const mBanner = await $(banner);
    await mBanner.waitForDisplayed(10000);
    await mBanner.click();
};

async function getStatus(url){
    response = await fetch(url);
    result = response.status;
    if(response.ok){
        console.log("IT'S ALIVE!!! " + result);
    }else{
        console.log("ERROR!!! " + result);
    }
};

describe('Главные страницы сайта доступны', () => {
    describe('RU', () => {
        beforeEach(async ()=> {
            await browser.reloadSession();
            await browser.setWindowSize(1024, 768);
        });
            
        it.skip('Статус код 200', async () => {

        });

        it('Стрнаица загрузилась', async () => {
            await clickingMainBanner(pageToFollow[0], mainBanner[0]);
        } );

    });

    describe('EN', () => {
        beforeEach(async ()=> {
            await browser.reloadSession();
            await browser.setWindowSize(1024, 768);
        });

        it.skip('Статус код 200', async () => {

        });

        it('Стрнаица загрузилась', async () => {
            await clickingMainBanner(pageToFollow[1], mainBanner[0]);
        } );
    });

    describe('UA', () => {
        beforeEach(async ()=> {
            await browser.reloadSession();
            await browser.setWindowSize(1024, 768);
        });

        it('Статус код 200', async () => {
            await getStatus(pageToFollow[2]);
            await assert.equal(result, statusCodeOk[0]);
        });

        it('Стрнаица загрузилась', async () => {
            await clickingMainBanner(pageToFollow[2], mainBanner[0]);
        } );
    });

    describe('M', () => {
        beforeEach(async ()=> {
            await browser.reloadSession();
            await browser.setWindowSize(415, 1000);
        });

        it('Статус код 200', async () => {
            await getStatus(pageToFollow[3]);
            await assert.equal(result, statusCodeOk[0]);
        });

        it('Стрнаица загрузилась', async () => {
            await clickingMainBanner(pageToFollow[3], mainBanner[1]);
        } );
    });
});
