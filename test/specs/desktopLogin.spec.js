const assert = require('assert');
let retailEmail = 'd.mazurenko@gepur.org';
let wholesaleEmail = 'gepur-test-opt@ukr.net';
let retailPassword = 'pKTDMGAXbqjT';
let wholesalePassword = 'pKTDMGAXbqjT';
let linkToFollow = [
  'https://gepur.com',
  'https://en.gepur.com',
  'https://ua.gepur.com'
];
let resultPageUrl = [
  'https://gepur.com/',
  'https://en.gepur.com/',
  'https://ua.gepur.com/'
];
let pageUrl;

async function fillInLoginForm(login, password){
  let loginField = await $('#loginform-email');
  await loginField.setValue(login);
  let passwordField = await $('#loginform-password');
  await passwordField.setValue(password);
  let submitButton = await $('button.btn-basik.btn-sm.btn-black');
  await submitButton.click();
};
async function performLoginRetail(){
  await fillInLoginForm(retailEmail, retailPassword);
  await browser.pause(1200);
  pageUrl = await browser.getUrl();
  await browser.pause(500);
}
async function performLoginWholesale(){
  await fillInLoginForm(wholesaleEmail, wholesalePassword);
  await browser.pause(1200);
  pageUrl = await browser.getUrl();
  await browser.pause(500);
}

describe('RU. Десктоп. Логин', () => {

  beforeEach(async ()=> {
    await browser.reloadSession();
    await browser.setWindowSize(1024,768);
    await browser.url(linkToFollow[0]);
    let loginButton = await $('a.h-account');
    await loginButton.click();
  });

  it('Розница', async ()=> {
    await performLoginRetail();
    await assert.equal(pageUrl, resultPageUrl[0]);
  });

  it('Опт', async ()=> {
    await performLoginWholesale();
    await assert.equal(pageUrl, resultPageUrl[0]);
  });
});

describe('EN. Десктоп. Логин', () => {

  beforeEach(async ()=> {
    await browser.reloadSession();
    await browser.setWindowSize(1024,768);
    await browser.url(linkToFollow[1]);
    let loginButton = await $('a.h-account');
    await loginButton.click();
  });

  it('Розница', async ()=> {
    await performLoginRetail();
    await assert.equal(pageUrl, resultPageUrl[1]);
  });

  it('Опт', async ()=> {
    await performLoginWholesale();
    await assert.equal(pageUrl, resultPageUrl[1]);
  });
});

describe('UA. Десктоп. Логин', () => {

  beforeEach(async ()=> {
    await browser.reloadSession();
    await browser.setWindowSize(1024,768);
    await browser.url(linkToFollow[2]);
    let loginButton = await $('a.h-account');
    await loginButton.click();
  });

  it('Розница', async ()=> {
    await performLoginRetail();
    await assert.equal(pageUrl, resultPageUrl[2]);
  });

  it('Опт', async ()=> {
    await performLoginWholesale();
    await assert.equal(pageUrl, resultPageUrl[2]);
  });
});