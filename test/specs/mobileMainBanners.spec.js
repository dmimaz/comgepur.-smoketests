const assert = require('assert');
const bannerId = [
  '//*[@id="root"]/div/div[4]/div/div[2]/div[1]/div[1]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[1]/div[2]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[2]/div[1]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[2]/div[2]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[4]/div[1]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[4]/div[2]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[4]/div[3]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[5]/div[1]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[5]/div[2]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[5]/div[3]/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[6]/div/a/img',
  '//*[@id="root"]/div/div[4]/div/div[2]/div[7]/div/a/img'
];
let bannerElement, linkToFollow;
const resultUrl = [
  'https://m.gepur.com/catalog/novinki?main_banner_1_101219',
  'https://m.gepur.com/bestidea/gepur-design?main_banner_2_101219',
  'https://m.gepur.com/catalog/platya?main_banner_3_101219',
  'https://m.gepur.com/catalog/verhnyaya-odezhda?main_banner_4_101219',
  'https://m.gepur.com/catalog/bluzy-rubashki?main_banner_5_101219',
  'https://m.gepur.com/catalog/futbolki-bluzy-svitera?main_banner_6_101219',
  'https://m.gepur.com/catalog/kostyumy-i-komplekty?main_banner_7_101219',
  'https://m.gepur.com/catalog/aksessuary?main_banner_8_101219',
  'https://m.gepur.com/catalog/sumki-klatchi-koshelki?main_banner_9_101219',
  'https://m.gepur.com/catalog/obuv?main_banner_10_101219',
  'https://m.gepur.com/bestidea/sedokova2019?main_banner_11_101219',
  'https://m.gepur.com/catalog/odezhda-dlya-doma?main_banner_12_101219'
];
const pageTitle = 'GEPUR: Женская одежда оптом и в розницу от производителя, официальный сайт интернет магазина Гепюр (Гепюр), продажа в Европе и странах СНГ, в России, Украине, Казахстане, Беларуси, Литве, Латвии';

async function clickingBanner(id){
  try{
    bannerElement = await $(id);
    await bannerElement.click();
    linkToFollow = await bannerElement.getUrl();
  }catch(error){
    throw(error)
  }
}

describe('Мобильный', () => {
  describe('Главная страница', () => {
      
    beforeEach(async () => {
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url('https://m.gepur.com/');
      await browser.pause(500);
    });

    it('m.gepur.com имеет праваильный title', async () => {
      const title = await browser.getTitle();
      assert.equal(title, pageTitle);
    });

    it('Баннер "Новинки" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[0]);
      await assert.equal(linkToFollow, resultUrl[0]);
    });

    it('Баннер "Gepur Desin" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[1]);
      await assert.equal(linkToFollow, resultUrl[1]);
    });

    it('Баннер "Плятья" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[2]);
      await assert.equal(linkToFollow, resultUrl[2]);
    });

    it('Баннер "Верхняя одежда" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[3]);
      await assert.equal(linkToFollow, resultUrl[3]);
    });

    it('Баннер "Боди и блузы" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[4]);
      await assert.equal(linkToFollow, resultUrl[4]);
    });

    it('Баннер "Худи и свитера" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[5]);      
      await assert.equal(linkToFollow, resultUrl[5]);
    });

    it('Баннер "Костюмы и комплекты" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[6]);
      await assert.equal(linkToFollow, resultUrl[6]);
    });

    it('Баннер "Аксессуары" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[7]);
      await assert.equal(linkToFollow, resultUrl[7]);
    });

    it('Баннер "Сумки, клатчи" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[8]);
      await assert.equal(linkToFollow, resultUrl[8]);
    });
    
    it('Баннер "Обувь" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[9]);
      await assert.equal(linkToFollow, resultUrl[9]);
    });
    
    it('Баннер "Седокова" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[10]);
      await assert.equal(linkToFollow, resultUrl[10]);
    });

    it('Баннер "Для дома и сна" ведет по правильному адресу', async () => {
      await clickingBanner(bannerId[11]);
      await assert.equal(linkToFollow, resultUrl[11]);
    }); 
  }); 
});