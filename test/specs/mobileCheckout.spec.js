
// var webdriverio = require('webdriverio');
        
// var options = { 
//     host: 'http://192.168.0.129:4444',
//     desiredCapabilities: { 
//         browserName: 'chrome', 
//         version: '78.0',
//         enableVNC: true,
//         enableVideo: false 
//     } 
// };
// var client = webdriverio.remote(options);

// User Data
const userName = [
  'ТЕСТ Розница ТЕСТ', 
  'ТЕСТ Опт ТЕСТ'
];
const userEmail = [
  'd.mazurenko@gepur.org', 
  'gepur-test-opt@ukr.net'
];
const password = [
  'pKTDMGAXbqjT', 
  'pKTDMGAXbqjT'
];
const userPhone = [
  '0671143458', 
  '0671143458'
];

// Other
const siteUrl = 'https://m.gepur.com/';
const comment = '!!!TEST!!!';

// Left Menu
const mainMenuBtn = '//*[@id="root"]/div/header/div[1]';
const userAccountBtn = '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[7]/div/a/span';
const mainMenuCloseBtn = '//*[@id="root"]/div/header/div[3]/aside/div[1]/div/button[1]/span[3]';

// Auth page
const emailInput = '#user-sign-in-email';
const passwordInput = '#user-password';
const submitBtn = '//*[@id="root"]/div/div[4]/div/div/div[2]/div/div[1]/div/form/div[3]/div/button';
const catalogLink = '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[2]/div/span';
const accessoriesLink = '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[4]/div/div[22]/div/a/span';

// Catalog
const productSlider = '//*[@id="grid"]/div[1]/div/a/div[1]/div';

// Product card
const productQualityInput = '//*[@id="root"]/div/div[4]/div/div/div[5]/div[3]/button[2]/div/input';
const addToCartBtn = '//*[@id="root"]/div/div[4]/div/div/div[5]/div[4]/div/button';

//Drover
const menuAddToCartBtn = '//*[@id="root"]/div/div[4]/div/div/div[1]/div/a/div/div/button';

//Checkout Step 1
const step1SubmitBtn = '//*[@id="root"]/div/div[4]/div/div[2]/div/a/div/div/button';

//Checkout Step 2
const userDataInput = '#customer-fio';
const customerAddressInput = '#customerform-address';
const cityString = 'Одесса';
const customerPhoneInput = '#customer-phone';
const customerEmailInput = '#customer-email';
const step2SubmitBtn = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[6]/div/button';

//Checkout Step 3
const chooseDeliveryBtn = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[3]/button/div/div[1]/span';
const novayaPochtaBtn = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[3]/div[1]/div/div/div[1]/div/span';
const step3SubmitBtn = '//*[@id="delivery-form"]/div[6]/div/button';

//Checkout Step 4
const paymentMethodBtn = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[5]/button/div/div[1]/span';
const pbBtn = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[5]/div[1]/div/div/div[2]/div/span';
const leaveCommentBtnRetail = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[9]/li/span';
const leaveCommentBoxTextareaRetail = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[9]/li/div/textarea';
const prformOrderBtnRetail = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[9]/div/div/button';

const leaveCommentBtnWholesale = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[9]/li/span';
const leaveCommentBoxTextareaWholesale = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[9]/li/div/textarea';
const prformOrderBtnWholesale = '//*[@id="root"]/div/div[4]/div/div[2]/div/div[9]/div/div/button';


async function reachingAuthPage(){
  try{
    const mainMenu = await $(mainMenuBtn);
    await mainMenu.click();
    const userAccount = await $(userAccountBtn);
    await userAccount.waitForDisplayed(10000);
    await userAccount.click();
    const mainMenuClose = await $(mainMenuCloseBtn);
    await userAccount.waitForDisplayed(10000);
    await mainMenuClose.click();
  }catch(error){
    throw(error)
  }
}

async function fillinginAuthForm(login, passwd){
  try{
    const userEmail = await $(emailInput);
    await userEmail.setValue(login);
    const userPasswd = await $(passwordInput);
    await userPasswd.setValue(passwd);
    const submit = await $(submitBtn);
    await submit.waitForDisplayed(10000);
    await submit.click();
  }catch(error){
    throw(error)
  }
}

async function addProductToCart(quantity){
  try{
    const mainMenu = await $(mainMenuBtn);
    await mainMenu.click();
    const catalog = await $(catalogLink);
    await catalog.click();
    const accessories = await $(accessoriesLink);
    await accessories.scrollIntoView();
    await accessories.click();
    const slider = await $(productSlider);
    await slider.click();
    const productQuality = await $(productQualityInput);
    await productQuality.clearValue();
    await productQuality.setValue(quantity);
    const cardSubmitBtn = await $(addToCartBtn);
    await cardSubmitBtn.click();
    await browser.pause(1000);
    const menuAddToCart = await $(menuAddToCartBtn);
    await menuAddToCart.waitForDisplayed(5000);
    await menuAddToCart.click();
  }catch(error){
    throw(error)
  }
}

async function proceedCheckoutStep1(){
  try{
    const step1Submit = await $(step1SubmitBtn);
    await step1Submit.waitForDisplayed(5000);
    await step1Submit.click();
  }catch(error){
    throw(error);
  }
}

async function proceedCheckoutStep2(name, city, phone, email){
  try{
    const userData = await $(userDataInput);
    await userData.setValue(name);
    const customerFormAddress = await $(customerAddressInput);
    await customerFormAddress.click();
    await customerFormAddress.clearValue();
    await browser.pause(1000);
    await customerFormAddress.setValue(city);
    await browser.pause(300);
    await browser.keys('ArrowDown');
    await browser.pause(300);
    await browser.keys('Enter');
    const customerPhone = await $(customerPhoneInput);
    await customerPhone.setValue(phone);
    const customerEmail = await $(customerEmailInput);
    await customerEmail.setValue(email);
    const nextBtnTwo = await $(step2SubmitBtn);
    await nextBtnTwo.scrollIntoView();
    await nextBtnTwo.click(); 
  }catch(error){
    throw(error);
  }
}

async function proceedCheckoutStep3(){
  try{
    await browser.pause(1000);
    const chooseDelivery = await $(chooseDeliveryBtn);
    await chooseDelivery.click();
    const novayaPochta = await $(novayaPochtaBtn);
    await novayaPochta.waitForDisplayed(15000);
    await novayaPochta.click();
    const nextButtonThree = await $(step3SubmitBtn);
    await nextButtonThree.waitForDisplayed(10000);
    await nextButtonThree.click();
  }catch(error){
    throw(error);
  }
}
async function proceedCheckoutStep4Retail(){
  try{
    const paymentMethod = await $(paymentMethodBtn);
    await browser.pause(1000);
    await paymentMethod.scrollIntoView();
    await paymentMethod.waitForDisplayed(10000);
    await paymentMethod.click();
    await browser.pause(3000);
    const pBCheck = await $(pbBtn);
    await pBCheck.click();
    await browser.pause(500);
    const leaveCommentBtn = await $(leaveCommentBtnRetail);
    await browser.pause(500);
    await leaveCommentBtn.click();
    const leaveCommentBox = await $(leaveCommentBoxTextareaRetail);
    await leaveCommentBox.setValue(comment);
    const prformOrderBtn = await $(prformOrderBtnRetail);
    await prformOrderBtn.click();
    await browser.pause(500);
  }catch(error){
    throw(error);
  }
}
async function proceedCheckoutStep4Wholesale(){
  try{
    const paymentMethod = await $(paymentMethodBtn);
    await browser.pause(1000);
    await paymentMethod.click();
    const pBCheck = await $(pbBtn);
    await pBCheck.click();
    await browser.pause(500);
    const leaveCommentBtn = await $(leaveCommentBtnWholesale);
    await browser.pause(500);
    await leaveCommentBtn.click();
    const leaveCommentBox = await $(leaveCommentBoxTextareaWholesale);
    await leaveCommentBox.setValue(comment);
    const prformOrderBtn = await $(prformOrderBtnWholesale);
    await prformOrderBtn.click();
    await browser.pause(500);
  }catch(error){
    throw(error);
  }
}

describe('Mobile. Checkout. New Post. Privat Bank card. ', () => {

  beforeEach(async () => {
    await browser.reloadSession();
    await browser.setWindowSize(414, 1000);
    await browser.url(siteUrl);
  });

  it('Retail', async () => {
    await reachingAuthPage();
    await fillinginAuthForm(userEmail[0], password[0]);
    await addProductToCart(2);
    await proceedCheckoutStep1();  
    await proceedCheckoutStep2(userName[0], cityString, userPhone[0], userEmail[0]);
    await proceedCheckoutStep3();
    await proceedCheckoutStep4Retail();
  });

  it('Wholesale', async () => {
    await reachingAuthPage();
    await fillinginAuthForm(userEmail[1], password[1]);
    await addProductToCart(5);
    await proceedCheckoutStep1();  
    await proceedCheckoutStep2(userName[1], cityString, userPhone[1], userEmail[1]);
    await proceedCheckoutStep3();
    await proceedCheckoutStep4Wholesale();
  });
});