const chai = require('chai');
const chaiWebdriver = require('chai-webdriverio').default;
chai.use(chaiWebdriver(browser));
global.assert = chai.assert;
expect = chai.expect;
const assert = require('assert');
let linkToFollow = 'https://m.gepur.com/';
let menuBtn = '//*[@id="root"]/div/header/div[1]/button';
let elementId = [
  '//*[@id="root"]/div/header/div[3]/aside/div[1]/div/button[1]',                   // Закрыть 
  '//*[@id="root"]/div/header/div[3]/aside/div[1]/div/button[2]',                   // Валюта
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[1]/div/span',   // Избранне
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[2]/div/span',   // Акции
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[3]/div/a/span', // Новинки
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[4]/div/span',   // Одежда
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[5]/div/a/span', // Моя корзина
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[6]/div/a/span', // Личный кабинет
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[8]/div/a/span', // Контакты
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[1]/div[9]/div/span',   // Помощь
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[3]/a[1]',              // ВК
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[3]/a[2]',              // Одноклассники
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[3]/a[3]',              // Ютуб
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[3]/a[4]',              // Инста
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[3]/a[5]',              // ФБ
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[3]/a[6]',              // Вайбер
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[3]/a[7]',              // Телеграм
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[4]/span[1]',           // Бесплатная
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[4]/span[2]',           // горячая линия
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[4]/a',                 // 0800...
  '//*[@id="root"]/div/header/div[3]/aside/div[2]/div[2]/div[5]/div/a'              // Полная версия сайта
];
let element;
let mainPageBanner = '//*[@id="header-home-page-link"]/div';

async function clickElement(id){
  openMenuBtn = await $(menuBtn);
  await openMenuBtn.click();
  element = await $(id);
  await browser.pause(300);
  await element.click();
}

describe('Мобильный. Главное меню.', () => {
  describe('Общий вид. Присутствие и функциональность всех элементов', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });
    
    it('Кнопка "Закрыть" доступна и функциональна', async () => {
      await clickElement(elementId[0]);
    });  
  });

  describe('Главное меню. Функционал. Меню "Валюта"', () => {
    
    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });
    
    it('Кнопка "Валюта" доступна и функциональна', async () => {
      await clickElement(elementId[1]);
    });  
  });  

  describe('Главное меню. Функционал. Меню "Избранное"', () => {
    
    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "Избранно" доступна и функциональна', async () => {
      await clickElement(elementId[2]);
    });
  });
  
  describe('Главное меню. Функционал. Меню "Акции"', () => {
    
    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "Акции" доступна и функциональна', async () => {
      await clickElement(elementId[3]);
    });
  });

  describe('Главное меню. Функционал. Меню "Новинки"', () => {
    
    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });
    
    it('Кнопка "Новинки" доступна и функциональна', async () => {
     await clickElement(elementId[4]);
    });
    
    it('Кнопка "Новинки". Редирект на https://m.gepur.com/catalog/novinki', async () => {
      await clickElement(elementId[4]);
      await browser.pause(300);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://m.gepur.com/catalog/novinki');
    });
  });

  describe('Главное меню. Функционал. Меню "Одежда"', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "Одежда" доступна и функциональна', async () => {
      await clickElement(elementId[5]);
    });
  });
  
  describe('Главное меню. Функционал. Меню "Моя корзина"', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "Моя корзина" доступна и функциональна', async () => {
      await clickElement(elementId[6]);
    });

    it('Кнопка "Моя корзина". Редирект на https://m.gepur.com/checkout/step-one', async () => {
      await clickElement(elementId[6]);
      await browser.pause(300);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://m.gepur.com/checkout/step-one');
    });
  });

  describe('Главное меню. Функционал. Меню "Личный кабинет"', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "Личный кабинет" доступна и функциональна', async () => {
      await clickElement(elementId[7]);
    });

    it('Кнопка "Личный кабинет". Редирект на https://m.gepur.com/auth/index', async () => {
      await clickElement(elementId[7]);
      await browser.pause(300);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://m.gepur.com/auth/index');
    });
  });

  describe('Главное меню. Функционал. Меню "Контакты"', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "Контакты" доступна и функциональна', async () => {
      await clickElement(elementId[8]);
    });

    it('Кнопка "Контакты". Редирект на https://m.gepur.com/auth/index', async () => {
      await clickElement(elementId[8]);
      await browser.pause(1000);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://m.gepur.com/info/contact');
    });
  });

  describe('Главное меню. Функционал. Меню "Помощь"', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "Помощь" доступна и функциональна', async () => {
      await clickElement(elementId[9]);
    });
  });

  describe('Главное меню. Функционал. Блок "Соцсети"', () => {
    
    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Кнопка "vk.com". Редирект на "https://vk.com/gepur"', async () => {
      await clickElement(elementId[10]);
      await browser.pause(5000);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://vk.com/gepur');
    });

    it('Кнопка "ok.ru". Редирект на "https://ok.ru/gepurcom?st._aid=ExternalGroupWidget_OpenGroup"', async () => {
      await clickElement(elementId[11]);
      await browser.pause(5000);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://ok.ru/gepurcom?st._aid=ExternalGroupWidget_OpenGroup');
    });

    it('Кнопка "youtube.com". Редирект на "https://www.youtube.com/channel/UCzd4UraIQcpMgbphm2Ig5LA"', async () => {
      await clickElement(elementId[12]);
      await browser.pause(5000);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://www.youtube.com/channel/UCzd4UraIQcpMgbphm2Ig5LA');
    });

    it('Кнопка "instagram.com". Редирект на "https://instagram.com/gepurofficial/"', async () => {
      await clickElement(elementId[13]);
      await browser.pause(5000);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://www.instagram.com/gepurofficial/');
    });

    it('Кнопка "facebook.com". Редирект на "https://www.facebook.com/gepurofficial/"', async () => {
      await clickElement(elementId[14]);
      await browser.pause(5000);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://www.facebook.com/gepurofficial/');
    });
    
    it('Кнопка "viber" доступна и функциональна', async () => {
      await clickElement(elementId[15]);
    });

    it('Кнопка "Telegram" доступна и функциональна', async () => {
      await clickElement(elementId[16]);
    });

    it('Кнопка "Telegram". Редирект на "https://t.me/gepurbot"', async () => {
      await clickElement(elementId[16]);  
      await browser.pause(1000);
      let pageUrl = await browser.getUrl();
      await expect(pageUrl).to.equal('https://t.me/gepurbot');
    });
  });

  describe('Главное меню. Функционал. Блок "Горячая линия"', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });
    
    it('Строка "Бесплатная" присутствует', async () => {
      await clickElement(elementId[17]);
      await browser.pause(5000);
      await element.isExisting();
    });

    it('Строка "горячая линия" присутствует', async () => {
      await clickElement(elementId[18]);
      await browser.pause(5000);
      await element.isExisting();
    });

    it('Строка "0 800 208-832" присутствует', async () => {
      await clickElement(elementId[19]);
      await browser.pause(5000);
      await element.isExisting();
    });
  });
  
  describe('Блок "Полная версия сайта" доступен', () => {

    beforeEach(async () => {
      await browser.setTimeout({ 'implicit': 20000 });
      await browser.reloadSession();
      await browser.setWindowSize(415, 1000);
      await browser.url(linkToFollow);
    });

    it('Ссылка в блоке кликабельна. Редирект на "https://gepur.com/?r=desktop"', async () => {
      await clickElement(elementId[20]);
      await browser.pause(5000);
      let mainBanner = await $(mainPageBanner);
      await mainBanner.isExisting();
    });
  });
});
