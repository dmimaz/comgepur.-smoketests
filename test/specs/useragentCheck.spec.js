const assert = require('assert');
const desktopUseragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36';
const mobileAndroidUserAgent = 'Mozilla/5.0 (Linux; Android 9; SM-G960F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36';
const mobileIosUserAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148';
const desktopUrl = 'https://gepur.com/';
const mobileUrl = 'https://m.gepur.com/';
const resultDesktopUrl = 'https://gepur.com/';
const resultMobileUrl = 'https://m.gepur.com/';

function setUserAgent(window, userAgent) {
  if (navigator.__defineGetter__) {
      navigator.__defineGetter__('userAgent', function () {
          return userAgent;
      });
  } else if (Object.defineProperty) {
      Object.defineProperty(navigator, 'userAgent', {
          get: function () {
              return userAgent;
          }
      });
  }
}

describe('Открылись страницы. соответствующие юзерагенту', () => {
  describe('Десктопный юзерагент', () => {
    beforeEach(async () => {
     // await setUserAgent(window, desktopUseragent);
     
    });

    it('RU. Открылся десктопный сайт', async () => {
      //let windowHandle = await browser.windowHandles();
      await browser.url(desktopUrl);
      await browser.pause(300);
      const pageUrl = await browser.getUrl();
      //await console.log(windowHandles);
      await browser.pause(500);
      assert.equal(pageUrl, resultDesktopUrl);
    });
  });
});