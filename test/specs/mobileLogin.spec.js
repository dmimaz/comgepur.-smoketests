const assert = require('assert');

const userEmail = [
  'd.mazurenko@gepur.org',
  'gepur-test-opt@ukr.net'
];
const userPassword = [
  'pKTDMGAXbqjT',
  'pKTDMGAXbqjT'
];
const linkToFollow = 'https://m.gepur.com/';
const resultPageUrl = 'https://m.gepur.com/';
const emailInput = '#user-sign-in-email';
const passwordInput = '#user-password';
const loginBtn = '#root > div > div.responsive-content__cmt > div > div > div.a-cnt__tabs.clearfix > div > div.a-tabs__item.a-tabs__list--active > div > form > div.btn-group > div > button';
const mainMenuBtn = '//*[@id="root"]/div/header/div[1]';
const userAccauntBtn = '=Личный кабинет';
const mainMenuCloseBtn = '#root > div > header > div:nth-child(4) > aside > div.g-menu__navbar > div > button.btn__h.btn__h--active';
let pageUrl;

async function fillInLoginForm(loginStr, passStr){
  try{
    const email = await $(emailInput);
    await email.setValue(loginStr);
    const password = await $(passwordInput);
    await password.setValue(passStr);
    const login = await $(loginBtn);
    await login.click();
  }catch(error){
    throw (error);
  };
}

describe('Mobile. Auth.', () => {

  beforeEach(async () => {
    await browser.reloadSession();
    await browser.setWindowSize(414, 1000);
    await browser.url(linkToFollow);
    await browser.pause(500);
  });

async function performTest(loginStr, passStr){
  try{
    const mainMenu = await $(mainMenuBtn);
    await mainMenu.click();
    await browser.pause(300);
    const userAccount = await $(userAccauntBtn);
    await userAccount.click();
    const mainMenuClose = await $(mainMenuCloseBtn);
    await mainMenuClose.click();
    await fillInLoginForm(loginStr, passStr);
    await browser.pause(20000);
    pageUrl = await browser.getUrl();
    await assert.equal(pageUrl, resultPageUrl);
  }catch(error){
    throw (error);
  };
};

  it('Retail', async ()=> {
    await performTest (userEmail[0], userPassword[0])
    await assert.equal(pageUrl, resultPageUrl);
  });

  it('Wholesale', async ()=> {
    await performTest (userEmail[1], userPassword[1])
    await assert.equal(pageUrl, resultPageUrl);
  });
});