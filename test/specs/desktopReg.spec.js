const assert = require('assert');

let startUrl = [
  'https://gepur.com',
  'https://en.gepur.com',
  'https://ua.gepur.com'
];
let finalUrl = [
  'https://gepur.com',
  'https://en.gepur.com',
  'https://ua.gepur.com'
];
let referenceUrl = [
  'https://gepur.com',
  'https://en.gepur.com',
  'https://ua.gepur.com'
];
let fioDef = [
  'ROZNITSA TEST TEST',
  'OPT TEST TEST'
];
let emailDef = [
  'd_' + 'd.mazurenko' + Math.floor((Math.random() * 99999999)) + '@gepur.org',
  'd_' + 'gepur-test-opt' + Math.floor((Math.random() * 99999999)) + '@ukr.net'
];
let passwordDef = [
  'Fklg64sd',
  'Fklg64sd'
];

let headerBtn = '//*[@id="r_header"]/header/div/div[1]/a[2]';
let regBtn1 = '//*[@id="app"]/main/div/div[2]/div[2]/div[2]/div/a/button';
let inputFio = '#customerform-fio';
let inputEmail = '#customerform-email';
let inputPassword = '#customerform-password';
let inputRadio = [
  '[value="retail_buyer"]',
  '[value="wholesale_buyer"]'
];
let regBtn2 = '[name="signup-button"]';
let element;

async function performAuthRetail(url, fio, email, password){
  await browser.url(url);
  element = await $(headerBtn);
  await element.click();
  element = await $(regBtn1);
  await element.click();
  element = await $(inputFio);
  await element.setValue(fio);
  element = await $(inputEmail);
  await element.setValue(email);
  element = await $(inputPassword);
  await element.setValue(password);
  element = await $(inputRadio[0]);
  await element.click();
  element = await $(regBtn2);
  await element.scrollIntoView();
  await element.click();
  await browser.pause(1500);
}

async function performAuthWholeSale(url, fio, email, password){
  await browser.url(url);
  element = await $(headerBtn);
  await element.click();
  element = await $(regBtn1);
  await element.click();
  element = await $(inputFio);
  await element.setValue(fio);
  element = await $(inputEmail);
  await element.setValue(email);
  element = await $(inputPassword);
  await element.setValue(password);
  element = await $(inputRadio[1]);
  await element.click();
  element = await $(regBtn2);
  await element.scrollIntoView();
  await element.click();
  await browser.pause(1500);
}

describe('Десктоп. Логин', () => {
  describe('RU', () => {
    
    beforeEach( async () => {
      await browser.reloadSession();
      await browser.setWindowSize(1024, 768);
    });

    it('Розница', async () => {
      await performAuthRetail(startUrl[0], fioDef[0], emailDef[0], passwordDef[0]);
      await assert.equal(finalUrl[0], referenceUrl[0]);
    });
    
    it('Опт', async () => {
      await performAuthWholeSale(startUrl[0], fioDef[1], emailDef[1], passwordDef[1]);
      await assert.equal(finalUrl[0], referenceUrl[0]);
    });
  });

  describe('EN', () => {
    
    beforeEach( async () => {
      await browser.reloadSession();
      await browser.setWindowSize(1024, 768);
    });

    it('Розница', async () => {
      await performAuthRetail(startUrl[1], fioDef[0], emailDef[0], passwordDef[0]);
      await assert.equal(finalUrl[1], referenceUrl[1]);
    });
    
    it('Опт', async () => {
      await performAuthWholeSale(startUrl[1], fioDef[1], emailDef[1], passwordDef[1]);
      await assert.equal(finalUrl[1], referenceUrl[1]);
    });
  });

  describe('UA', () => {
    
    beforeEach( async () => {
      await browser.reloadSession();
      await browser.setWindowSize(1024, 768);
    });

    it('Розница', async () => {
      await performAuthRetail(startUrl[2], fioDef[0], emailDef[0], passwordDef[0]);
      await assert.equal(finalUrl[2], referenceUrl[2]);
    });
    
    it('Опт', async () => {
      await performAuthWholeSale(startUrl[2], fioDef[1], emailDef[1], passwordDef[1]);
      await assert.equal(finalUrl[2], referenceUrl[2]);
    });
  });
});