var assert = require('assert');

try{
  describe('Десктоп. Каталог', () => {
    describe('Миниатюра товара', () => {
  
      beforeEach(async () => {
        await browser.reloadSession();
        await browser.setWindowSize(1024, 768);
        await browser.url('https://gepur.com/catalog/aksessuary');
      });
  
      it('Hover на товар работает', async () => {
        let firstSlider = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a/img');
        await firstSlider.moveTo();
        let sliderLeftArrow = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div');
        let sliderRighttArrow = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div');
        await sliderLeftArrow.isExisting();
        await sliderRighttArrow.isExisting();
      });
  
      it('Hover на товар. Левая стрелка присутствует', async () => {
        let firstSlider = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a/img');
        await firstSlider.moveTo();
        let sliderLeftArrowBtn = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div');
        await sliderLeftArrowBtn.isExisting();
      });
  
      it('Hover на товар. Правая стрелка присутствует', async () => {
        let firstSlider = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a/img');
        await firstSlider.moveTo();
        let sliderRighttArrowBtn = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div');
        await sliderRighttArrowBtn.isExisting();
      });
  
      it('Hover на товар. Кнопка "Быстрый заказ" присутствует', async () => {
        let firstSlider = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a/img');
        await firstSlider.moveTo();
        let fastOrderBtn = await $('#app > div > div > section > div > div > div.c-container__right--column > div.main_catalog.clearfix > div:nth-child(1) > span');
        await fastOrderBtn.scrollIntoView();
      });
  
      it('Клик по левой "стрелке" - перелистывание к следующей фотографии', async () => {
        let firstSlider = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/a/img');
        await firstSlider.moveTo();
        let sliderLeftArrowBtn = await $('//*[@id="app"]/div/div/section/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div');
        await sliderLeftArrowBtn.click();
        let sliderPhoto1 = $('');
      });
    });
  });
}catch(error){
  throw (error);
}